package com.challenge.filmku_id.repository;

import com.challenge.filmku_id.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UsersRepository extends JpaRepository<Users, Integer> {

    @Modifying
    @Query("select u from users u where u.userId =:userId")
    public Users findByUserId(
            @Param("userId") Integer userId);

    @Modifying
    @Query(value = "update users u set u.username= :username, u.email= :email, u.password= :password " +
            "where u.user_id= :user_id", nativeQuery = true)
    void updateUser(
            @Param("username") String username,
            @Param("email") String email,
            @Param("password") String password,
            @Param("user_id") Integer userId
    );

    @Modifying
    @Query("delete from users u where u.userId =:userId")
    public void deleteUsersByUserId(
            @Param("userId")Integer userId);

    public Users findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);

}