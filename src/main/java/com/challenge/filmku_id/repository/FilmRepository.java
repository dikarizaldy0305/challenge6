package com.challenge.filmku_id.repository;

import com.challenge.filmku_id.model.Films;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional
public interface FilmRepository extends JpaRepository<Films, Integer>{

    @Modifying
    @Query(value = "select * from films f where f.nama_film =:namaFilm", nativeQuery = true)
    List<Films> findFilmsByNamaFilm(@Param("namaFilm") String namaFilm);


    @Modifying
    @Query(value = "update films f set f.film_code= :film_code, f.nama_film= :nama_film, f.status_tayang= :status_tayang " +
            "where f.film_id= :film_id", nativeQuery = true)
    void updateFilm(@Param("film_code") String filmCode,
                    @Param("nama_film") String namaFilm,
                    @Param("status_tayang") String statusTayang,
                    @Param("film_id") Integer filmId);

    @Modifying
    @Query(value = "delete from films where film_id =:film_id", nativeQuery = true)
    void deleteFilm(@Param("film_id") Integer filmId);

}