package com.challenge.filmku_id.repository;

import com.challenge.filmku_id.model.Seats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

    @Repository
    @Transactional
    public interface SeatRepository extends JpaRepository<Seats, Integer> {

        @Modifying
        @Query(value = "select * from schedules sc " +
                "join seats s on s.schedules_id = sc.schedules_id " +
                "where s.nama_studio=:namaStudio", nativeQuery = true)
        public List<Seats> findByNamaStudio (String namaStudio);
    }
