package com.challenge.filmku_id.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity(name = "seats")
@IdClass(SeatsId.class)
public class Seats implements Serializable {

    @Id
    private String namaStudio;

    @Id
    private String nomorKursi;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = " schedules_id")
    private Schedules schedulesId;
}


