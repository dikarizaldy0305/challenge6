package com.challenge.filmku_id.service;

import com.challenge.filmku_id.model.Films;
import com.challenge.filmku_id.model.Schedules;
import com.challenge.filmku_id.model.Seats;
import com.challenge.filmku_id.repository.FilmRepository;
import com.challenge.filmku_id.repository.ScheduleRepository;
import com.challenge.filmku_id.repository.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FilmServiceImpl implements FilmService, ScheduleService, SeatService {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private SeatRepository seatRepository;


    @Override
    public void saveFilm(String filmCode, String namaFilm, String statusTayang) {
        Films films = new Films();
        films.setFilmCode(filmCode);
        films.setNamaFilm(namaFilm);
        films.setStatusTayang(statusTayang);
        filmRepository.save(films);
    }

    @Override
    public void updateFilm(String filmCode, String namaFilm, String statusTayang, Integer filmId){
        filmRepository.updateFilm(filmCode, namaFilm, statusTayang, filmId);
    }

    @Override
    public void deleteFilm(Integer filmId){
        filmRepository.deleteFilm(filmId);
    }

    @Override
    public List<Films> findFilmsByNamaFilm(String namaFilm) {
        return filmRepository.findFilmsByNamaFilm(namaFilm);
    }

    @Override
    public List<Films> filmsList(){
        return filmRepository.findAll();
    }

    @Override
    public List<Schedules> getByNamaFilm(String namaFilm) {
        return scheduleRepository.findByNamaFilm(namaFilm);
    }

    @Override
    public Schedules saveSchedule(Integer hargaTiket, String jamMulai, String jamSelesai, String tanggalTayang, Integer filmId) {
        Schedules schedule = new Schedules();
        schedule.setHargaTiket(hargaTiket);
        schedule.setJamMulai(jamMulai);
        schedule.setJamSelesai(jamSelesai);
        schedule.setTanggalTayang(tanggalTayang);
        Films film = new Films();
        film.setFilmId(filmId);
        schedule.setFilmId(film);
        scheduleRepository.save(schedule);
        return schedule;
    }

    @Override
    public List<Schedules> schedulesList() {
        return scheduleRepository.findAll();
    }


    @Override
    public List<Seats> getByNamaStudio(String namaStudio) {
        return seatRepository.findByNamaStudio(namaStudio);
    }
}
