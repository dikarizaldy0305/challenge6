package com.challenge.filmku_id.service;

import com.challenge.filmku_id.model.Users;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface UserService {
    public void saveUser(String username, String password, String email);
    public void updateUser(String username, String email, String password, Integer userId);
    public void deleteUsers(Integer userId);
    public List<Users> userList();
    public Users getUsersByUsername(String username);
}