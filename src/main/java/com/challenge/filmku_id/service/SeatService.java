package com.challenge.filmku_id.service;

import com.challenge.filmku_id.model.Seats;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SeatService {
    public List<Seats> getByNamaStudio(String namaStudio);
}
