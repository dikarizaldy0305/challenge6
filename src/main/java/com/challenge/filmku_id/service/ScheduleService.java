package com.challenge.filmku_id.service;

import com.challenge.filmku_id.model.Schedules;
import com.challenge.filmku_id.model.SeatsId;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ScheduleService {
    public List<Schedules> getByNamaFilm(String namaFilm);
    public Schedules saveSchedule(Integer hargaTiket, String jamMulai, String jamSelesai, String tanggalTayang, Integer filmId);

    List<Schedules> schedulesList();
}