package com.challenge.filmku_id.controller;

import com.challenge.filmku_id.model.Users;
import com.challenge.filmku_id.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Tag(name="Users", description = "API for processing with Users entity")
@RestController
@RequestMapping("/users")
public class UsersController {

    private static final Logger LOG = LoggerFactory.getLogger(UsersController.class);

    @Autowired
    private UserService userService;

    @PostMapping("/public/add-user")
    @Operation(summary = "Add a new user")
    @ApiResponse(responseCode = "211", description = "Success added a new user")
    public ResponseEntity addUser(@Schema(example = "{" + "\"userId\":\"userId\","+
            "\"username\":\"dika\","+
            "\"password\":\"dika123\"," +
            "\"email\":\"dika@gmail.com\"}")
                              @RequestBody Map<String, Object> user){
            userService.saveUser(user.get("username").toString(), user.get("email").
                    toString(), user.get("password").toString());

            Map<String, Object> responBody = new HashMap<>();
            responBody.put("username",user.get("username"));
            responBody.put("email",user.get("email"));
            responBody.put("password",user.get("password"));

            MultiValueMap<String, String> headers = new HttpHeaders();
            headers.put("Binar", Arrays.asList("Test"));
            return new ResponseEntity(responBody, headers, 211);
        }

//    @PreAuthorize("hasAuthority('CUSTOMER')")
    @GetMapping("/customer/show-user-by-username/{username}")
    @ApiResponse(responseCode = "212", description = "Success show all user (ByUsername)")
    @Operation(summary = "Show user by username")
    public List<Users> getUsersByUsername(@Schema(example = "{\"username\":\"Dika\"}")
                                              @PathVariable("username") String userName) {
        List<Users> users = (List<Users>) userService.getUsersByUsername(userName);
        return users;
    }

    @ApiResponse(responseCode = "213", description = "Success updated an user")
    @Operation (summary = "Updates an existing user")
    @PutMapping("/customer/update-user")
    public ResponseEntity updateUser(@Schema(example = "{" +"\"username\":\"dika\","+
            "\"email\":\"dika@gmail.com\"," +
            "\"password\":\"dika123\"," +
            "\"userId\":\"1\"}")
                               @RequestBody Map<String, Object> user){
        userService.updateUser(user.get("username").toString(), user.get("email").
                toString(), user.get("password").toString(), Integer.valueOf(user.get("userId").toString()));

        Map<String, Object> responBody = new HashMap<>();
        responBody.put("username",user.get("username"));
        responBody.put("email",user.get("email"));
        responBody.put("password",user.get("password"));
        responBody.put("userId",user.get("userId"));

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("Test"));
        return new ResponseEntity(responBody, headers, 212);
    }

    @ApiResponse(responseCode = "214", description = "Success deleted an user")
    @Operation(summary = "Deletes an user")
    @DeleteMapping("/customer/delete-user")
    public void deleteUser(@Schema(example = "{\"userId\":\"userId\"}")@RequestBody Users user){
        userService.deleteUsers(user.getUserId());
    }

    @ApiResponse(responseCode = "215", description = "Success show all registered users")
    @Operation(summary = "Show all registered users")
    @GetMapping(value = "/get-all-user")
    public void allUsers(){
        List<Users> usersList = userService.userList();
        usersList.forEach(users -> {
            System.out.println("userId : "+users.getUserId()+"\nusername : "+users.getUsername()+
                    "\nemail : "+users.getEmail()+"\npassword : "+users.getPassword()+"\n" );
        });
    }
}
