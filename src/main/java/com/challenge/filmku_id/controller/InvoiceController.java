package com.challenge.filmku_id.controller;

import com.challenge.filmku_id.model.*;
import com.challenge.filmku_id.service.FilmService;
import com.challenge.filmku_id.service.ScheduleService;
import com.challenge.filmku_id.service.SeatService;
import com.challenge.filmku_id.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Tag(name="Invoice", description = "API for processing with Invoice")
@RestController
@RequestMapping("/invoice")
public class InvoiceController {
    @Autowired
    private FilmService filmService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private UserService userService;

    @Autowired
    private SeatService seatService;

    @ApiResponses(value = {
            @ApiResponse( content = {
                    @Content(examples = {})
            }, responseCode = "234", description = "Success generated ticket invoice")
    })
    @Operation(summary = "Print invoice ticket that required Users, Films, Schedules and Seats Entity")
    @GetMapping("/customer/print-invoice")
    public ResponseEntity getInvoice(HttpServletResponse response) throws IOException, JRException {
        JasperReport sourceFileName = JasperCompileManager
                .compileReport(ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX
                        + "Tes.jrxml").getAbsolutePath());

//      creating our list of beans
        List<Map<String, String>> dataList = new ArrayList<>();

        Map<String, String> data = new HashMap<>();
        Films fl = filmService.filmsList().get(0);
        data.put("namaFilm", fl.getNamaFilm());
        Schedules sc = scheduleService.getByNamaFilm("Conan").get(0);
        data.put("hargaTiket", sc.getHargaTiket().toString());
        data.put("tanggalTayang",sc.getTanggalTayang().toUpperCase());
        data.put("jamMulai",sc.getJamMulai());
        data.put("jamSelesai",sc.getJamSelesai());
        Users us = userService.userList().get(0);
        data.put("pembeli",us.getUsername());
        Seats seats = seatService.getByNamaStudio("A").get(0);
        data.put("namaStudio", seats.getNamaStudio());
        data.put("nomorKursi", seats.getNomorKursi());
        dataList.add(data);

//      creating datasource from bean list
        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataList);
        Map<String, Object> parameters = new HashMap();
        parameters.put("createdBy", "Andika Rizaldy");
        JasperPrint jasperPrint = JasperFillManager.fillReport(sourceFileName, parameters, beanColDataSource);

        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "inline; filename=invoice.pdf;");
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());

        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
